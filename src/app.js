import React, { Component, Fragment } from 'react'
import { Requester } from './api/'


export class App extends Component {
  state = {
    value: '',
    response: {},
    fetching: false,
    error: null,
  }

  handleChange = (e) => {
    const { value } = e.target

    this.setState({ value })
  }

  handleSubmit = () => {
    const { value: username } = this.state

    this.setState({ fetching: true, value: '' })
    Requester.get(`/users/${username}`)
      .then(({ data }) => this.setState({ response: data, fetching: false }))
      .catch((error) => this.setState({ error, fetching: false }))
  }

  render() {
    const { fetching, response, value, error } = this.state

    return (
      <Fragment>
        <input type="text" onChange={this.handleChange} value={value} />
        <button onClick={this.handleSubmit} type="submit">Search</button>

        {fetching && <div>Loading</div>}
        {error && <div>Got error: {error.message}</div>}
        {!error && response && (
          <div id='response'>
            {Object.entries(response).map((item) => {
              const [key, val] = item

              return (
                <div key={key}>
                  {/url/.test(key) ? (
                    <Fragment>
                      {key}: <a className='more-data' href={val}>{val}</a>
                    </Fragment>
                  ) : (
                    <Fragment>
                      {key}: {val}
                    </Fragment>
                  )}
                </div>
              )
            })}
          </div>
        )}
      </Fragment>
    )
  }
}
